import { NextApiRequest, NextApiResponse } from 'next'
import { MongoClient, Db } from 'mongodb'
import { getSession } from 'next-auth/client'

let cachedDb: Db = null

async function connectToDatabase() {
    if(cachedDb){
        return cachedDb
    }

    const client = await MongoClient.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    const db = client.db('TodoDB')

    cachedDb = db

    return db
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const db = await connectToDatabase()

    const collection = db.collection('users')
    
    if(req.method == 'POST'){
        const todoBody = req.body

        await collection.insertOne({
            todo: {
                todoBody
            }
        })
    }
    if(req.method == 'GET'){
        const session = await getSession()
    
        const data = await collection.find({ userEmail: session.user.email }).toArray()
        
        return res.json({ data })
    }

}