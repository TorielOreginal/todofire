import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import axios from 'axios'

import { HeaderContainer, MainContaner, OrangeButton } from '../styles/styles'
import { signIn, signOut, useSession } from 'next-auth/client'
import Todo from '../styles/Todo'

const Home = () => {
    // const [todos, setTodos] = useState([])

    const [session] = useSession()

    // useEffect(() => {
    //    async function fetchTodos(){
    //        setTodos(await axios.get('/api/users'))
    //    }
        
    //    fetchTodos()
    //    console.log(todos)
    // })

    // async function createTodo(){

    // }

    return (
        <>
        <Head><title>TodoFire</title></Head>

        <HeaderContainer>
            <h2>Todo<strong>Fire</strong>.</h2>
            {
                !session && (
                    <button onClick={() => signIn()}>Login</button>
                )
            }

            {
                session && (
                    <>
                    <h3>Logged in as {session.user.name}</h3>
                    <button onClick={() => signOut()}>Logout</button>
                    </>
                )
            }
        </HeaderContainer>

        <MainContaner>
            {
                session && (
                    <OrangeButton style={{ marginBottom: 25 }}>New todo</OrangeButton>
                )
            }
            <h2>Your todos:</h2>

            {
                !session && (
                    <h3>You must be signed in to see your todos.</h3>
                )
            }

            {
                session && (
                    <>
                    <Todo name="Limpar a casa" />
                    </>
                )
            }
        </MainContaner>
        </>
    );
}

export default Home;