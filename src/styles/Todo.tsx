import React from 'react'
import styled from 'styled-components'
import { OrangeButton } from './styles'

const TodoContainer = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 10px;
`

const TodoCheckbox = styled.input`
    margin-right: 10px;
    &:checked{
        background-color: #FF5F19;
    }
`

interface TodoProps{
    name: string
}

const Todo: React.FC<TodoProps> = props => {
    return (
        <TodoContainer>
            <TodoCheckbox type="checkbox" />
            <span style={{ marginRight: 10 }}>{props.name}</span>
            <OrangeButton>Delete</OrangeButton>
        </TodoContainer>
    );
}
 
export default Todo;