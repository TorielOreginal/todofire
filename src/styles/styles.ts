import styled from 'styled-components'

export const HeaderContainer = styled.header`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: 25px;

    button{
        width: 100px;
        height: 40px;
        border: none;
        border-radius: 25px;
        background-color: #FF5F19;

        transition: .5s;
    }

    button:hover{
        width: 125px;
        cursor: pointer;
    }

    h2{
        font-family: 'Sora', sans-serif;
    }

    h2 strong{
        font-family: 'Sora', sans-serif;
        color: #FF5F19;
    }
`

export const MainContaner = styled.main`
    padding: 25px;
`

export const OrangeButton = styled.button`
    width: 100px;
    height: 40px;
    border: none;
    border-radius: 25px;
    background-color: #FF5F19;

    transition: .5s;

    &:hover{
        width: 125px;
        cursor: pointer;
    }
`